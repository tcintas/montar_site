<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use LaraDev\Http\Controllers\PropertyController;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/montarsite','PropertyController@index');

Route::get('/montarsite/novo', 'PropertyController@create');
Route::post('/montarsite/store', 'PropertyController@store');

Route::get('/montarsite/{client}', 'PropertyController@show');

Route::get('/montarsite/editar/{client}', 'PropertyController@edit');
Route::put('/montarsite/update/{client}', 'PropertyController@update');

Route::get('/montarsite/remover/{client}', 'PropertyController@destroy');


