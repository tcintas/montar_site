<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PropertiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('properties')->insert([
            'id' => 1,
            'full_name' => 'Cliente Teste',
            'client' => 'cliente-teste'
        ]);
    }
}
