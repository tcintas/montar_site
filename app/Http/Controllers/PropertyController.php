<?php

namespace MontarSite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use MontarSite\Property;


class PropertyController extends Controller
{
    public function index ()
    {
        $properties = Property::all();
        
        return view ('property.index')->with('properties', $properties);
    }

/*___________________________________________________________________________________*/


    public function show ($client)
    {
        
        $property = Property:: where ('client', $client)->get();

        if(!empty($property)){
           return view ('property.show')->with('property', $property);
        }   else {
                return redirect()->action('PropertyController@index');
            }    
    }

/*___________________________________________________________________________________*/



    public function create ()
    {
        return view ('property.create');
    }

    public function store (Request $request)
    {   
        $propertySlug = $this->setName($request->full_name);
        
      

        $property = [
               'full_name'=> $request->full_name,
               'client'=> $propertySlug,                       
        ];

        if (str_word_count($request->full_name) < 2){
            $msg = '<div class="alert alert-danger">Preencha o campo com nome e sobrenome.</div>';

            return view('property.create')->with('mensagem' , $msg);                     

        } else {

        
        Property::create($property);
                  
        return redirect()->action('PropertyController@index');
        }  
    }

    public function edit ($client){
        
        $property = Property:: where ('client', $client)->get();
        
        if(!empty($property)){
           return view ('property.edit')->with('property', $property);
        }   else {
                return redirect()->action('PropertyController@index');
            }   

    }

    public function update (Request $request, $id){

        $propertySlug = $this->setName($request->full_name);

        $property = Property::find($id);

        $property->full_name = $request->full_name;
        $property->client = $propertySlug;
        
        if (str_word_count($request->full_name) < 2){
            echo '<div class="alert alert-danger">Preencha o campo com nome e sobrenome.</div>';

            if(!empty($property)){
            return view('property.edit')->with('property', $property);                    } 

        } else {
        

        $property->save();

        return redirect()->action('PropertyController@index');
        }

    }
/*___________________________________________________________________________________*/
    public function destroy($client) {

        $property = Property:: where ('client', $client)->get();
       
        if(!empty($property)){
            DB::delete("DELETE FROM properties WHERE client = ?", [$client]);
        }    
        return redirect()->action('PropertyController@index');          
    }

/*___________________________________________________________________________________*/    
    private function setName($full_name)
    {
        $propertySlug = str_slug ($full_name);

        $properties = Property::all();

        $t = 0;
        foreach($properties as $property){
            if(\str_slug($property->full_name) === $propertySlug){
                $t++;
            }
        }

        if($t > 0){
            $propertySlug = $propertySlug . '-' . $t;
        }

        return $propertySlug;
    }

/*___________________________________________________________________________________*/    

}
