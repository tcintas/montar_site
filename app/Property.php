<?php

namespace MontarSite;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
 protected $table = 'properties';
 
 protected $fillable = ['full_name', 'client'];

 public $timestamps = true;
}
