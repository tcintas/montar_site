@extends('property.master')


@section('content')
<div class="container my-3">
    <h1>Formulário de Edição de Clientes</h1>

    <?php 
    $property = (isset($property->id)) ? $property : $property[0];
    ?>

    <script type="text/javascript">

        function validar(){
            var full_name = editclient.full_name.value;
                if(full_name == ""){
                    $('#feedback').html('<div class="alert alert-danger" role="alert">Preencha o campo com nome e sobrenome.</div>');
                    editclient.full_name.focus();
                    return false;
                }
        }
    </script>
    <div id='feedback'></div>
    <form name="editclient" action="<?= url('/montarsite/update', ['id' => $property->id]);?>" method="post">

        <?= csrf_field(); ?>
        <?= method_field('PUT'); ?>

        <div class="text-left col-sm-6">
            <div class="form-group">            
            <label for="full_name">Nome completo do Cliente</label>
            <input type="text" name="full_name" id="full_name" value="<?= $property->full_name; ?>"class="form-control">
            </div>
            
            <button type="submit" onclick="return validar()" class="btn btn-warning my-2">Editar Cliente</button>
            <a class="btn btn-secondary" href="<?= url('/montarsite'); ?>" role="button">Voltar</a>
            

        </div>

    </form>
</div>


@endsection