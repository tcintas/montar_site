@extends('property.master')


@section('content')
    
<div class="container my-3">
<h1>Listagem de Clientes</h1>

<?php       

if(!empty($properties)) {

    echo "<table class= 'table table-striped table-hover table-sm table-responsive-md'>";

    echo "<thead class = 'bg-warning text-black font-weight-bold'> 
                <td>Nome</td>
                <td>Ações</td>    
        </thead>";

    foreach ($properties as $property){

        $linkReadMore = url('/montarsite/' . $property->client);
        $linkEditItem = url('/montarsite/editar/' . $property->client);
        $linkRemoveItem = url('/montarsite/remover/' . $property->client);
        


        echo "<tr> 
                <td>{$property->full_name}</td>
                <td><a href= '$linkReadMore'>Ver Mais</a> | <a href= '$linkEditItem'>Editar</a> | <a href= '$linkRemoveItem'>Remover</a></td>
            </tr>";
    }

}

?>
</div>
@endsection