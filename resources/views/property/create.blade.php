@extends('property.master')


@section('content')
		
<div class="container my-3">
    <h1>Formulário de Cadastro de Clientes</h1>

    <script type="text/javascript">

        function validar(){
            var full_name = addclient.full_name.value;
                if(full_name == ""){
                    $('#feedback').html('<div class="alert alert-danger" role="alert">Preencha o campo com nome e sobrenome.</div>');
                    addclient.full_name.focus();
                    return false;
                }
        }
    </script>
    <div id='feedback'><?php echo (isset($mensagem)) ? $mensagem : ''; ?></div>
    <form name="addclient" action="<?= url('/montarsite/store');?>" method="post">

    <?= csrf_field(); ?>

            <div class="text-left col-sm-6">
            <div class="form-group">
                
            <label for="full_name">Nome completo do Cliente</label>
            <input type="text" name="full_name" id="full_name" class="form-control">
            </div>

            <button type="submit" onclick="return validar()" class="btn btn-warning my-2">Cadastrar Cliente</button>
            <a class="btn btn-secondary" href="<?= url('/montarsite'); ?>" role="button">Voltar</a>
        </div>
        
        
    </form>

</div>
@endsection