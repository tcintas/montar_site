<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>MontarSite Teste</title>
    {{-- IMPORTANDO CSS/BOOTSTRAP     --}}
    <link rel="stylesheet" href="<?= asset('css/app.css'); ?>">
</head>
<body>

<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container">
        <a href="<?= url('/montarsite'); ?>" class="navbar-brand">
            <font color="#A4A4A4">Montar</font><font color="#E7C91B"><b>Site</b></font>          
        </a>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item"><a href="<?= url('/montarsite'); ?>" class="nav-link">Listar Clientes</a></li>
            <li class="nav-item"><a href="<?= url('/montarsite/novo'); ?>" class="nav-link">Cadastrar Clientes</a></li>
        </ul>
    </div>
</nav>

@yield('content')
 {{-- IMPORTANDO JS --}}
<script>src="<?= asset('js/app.js'); ?>"</script>
<script   src="https://code.jquery.com/jquery-3.4.1.min.js"   integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="   crossorigin="anonymous"></script>

</body>
</html>

